import fetch from './fetch'

import config from '../../config'

const fetchAppServices = (verb = 'GET', path, body) => {
  let ws = config.ws

  return fetch(verb, `${ws.protocol}://${ws.url}/${path}`, body)
}

module.exports = fetchAppServices
