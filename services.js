/*
* Author: Tony Archer
* Creation Date: 9/11/2017
*
* The imported fetch function wraps error handling for html statuses >= 400
*
* Example Usage:
* let userFromToken = body =>           _fetch('POST', 'add-user', body)
* let getAvatar = email =>              _fetch('GET', `a/${email}`)
* let updateLink = (projectId, body) => _fetch('PATCH', `m/${projectId}`, body)
* */
import fetch from './fetch-app-service'
import error from '../../components/LightBox/errorHandler'


let getProjects = slug =>                               fetch('GET', `api/v1/projects/${slug}`)
let addProject = body =>                                fetch('POST', 'api/v1/projects', body)
let updateProject = (slug, body) =>                     fetch('PATCH', `api/v1/projects/${slug}`, body)
let removeProject = slug =>                             fetch('DELETE', `api/v1/projects/${slug}`)

let addKeyPlayer = (slug, body) =>                      fetch('POST', `api/v1/projects/${slug}/key-players`, body)
let updateKeyPlayer = (slug, keyPlayerId, body) =>      fetch('PATCH', `api/v1/projects/${slug}/key-players/${keyPlayerId}`, body)
let removeKeyPlayer = (slug, keyPlayerId) =>            fetch('DELETE', `api/v1/projects/${slug}/key-players/${keyPlayerId}`)

let getResourceLink = slug =>                           fetch('GET', `api/v1/projects/${slug}/resources`)
let addResourceLink = (slug, body) =>                   fetch('POST', `api/v1/projects/${slug}/resources`, body)
let updateResourceLink = (slug, resourceId, body) =>    fetch('PATCH', `api/v1/projects/${slug}/resources/${resourceId}`, body)

let getFileLink = slug =>                               fetch('GET', `api/v1/projects/${slug}/urls`)
let addFileLink = (slug, body) =>                       fetch('POST', `api/v1/projects/${slug}/urls`, body)
let updateFileLink = (slug, urlId, body) =>             fetch('PATCH', `api/v1/projects/${slug}/urls/${urlId}`, body)
let removeFileLink = (slug, urlId) =>                   fetch('DELETE', `api/v1/projects/${slug}/urls/${urlId}`)

let getTagSuggestions = () =>                           fetch('GET', 'api/v1/tags')
let updateTags = (slug, body) =>                        fetch('PATCH', `api/v1/tags/${slug}`, body)

let getUsers = encodedQuery =>                          fetch('GET', `api/v1/users?${encodedQuery}`)

module.exports = {
  project: {
    get: getProjects,
    add: addProject,
    update: updateProject,
    remove: removeProject
  },
  keyPlayers: {
    add: addKeyPlayer,
    update: updateKeyPlayer,
    remove: removeKeyPlayer
  },
  tags: {
    getSuggestions: getTagSuggestions,
    update: updateTags
  },
  user: {
    find: getUsers
  },
  resourceLink: {
    get: getResourceLink,
    add: addResourceLink,
    update: updateResourceLink
  },
  fileLinks: {
    get: getFileLink,
    add: addFileLink,
    update: updateFileLink,
    remove: removeFileLink
  },
  showError: error.show
}
