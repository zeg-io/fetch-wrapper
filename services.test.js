jest.mock('./fetch-app-service')

import services from './services'

describe('Test services calls', () => {
  it('calls a thing', () => {
    services.project.get('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.project.add('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.project.update('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.project.remove('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))

    services.keyPlayers.add('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.keyPlayers.update('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.keyPlayers.remove('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))

    services.tags.getSuggestions('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.tags.update('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))

    services.user.find('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))

    services.resourceLink.get('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.resourceLink.add('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.resourceLink.update('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))

    services.fileLinks.get('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.fileLinks.add('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.fileLinks.update('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
    services.fileLinks.remove('').then(r => true).catch(e => false).then(success => expect(success).toBe(true))
  })
})
