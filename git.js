import fetch from './fetch'
import error from '../../components/LightBox/errorHandler'

const gitApi = 'https://server.com/api/v3'

const basicProjectDetails = ownerProject =>     fetch('GET', `${gitApi}/repos/${ownerProject}`)
const stats = (ownerProject, apiCall) =>        fetch('GET', `${gitApi}/repos/${ownerProject}/stats/${apiCall}`)
const punchCard = ownerProject =>               stats(ownerProject, 'punch_card')
const participation = ownerProject =>           stats(ownerProject, 'participation')
const contributors = ownerProject =>            stats(ownerProject, 'contributors')
const releases = ownerProject =>                fetch('GET', `${gitApi}/repos/${ownerProject}/releases`)
const branches = ownerProject =>                fetch('GET', `${gitApi}/repos/${ownerProject}/branches`)

const buildApiCallPromiseArray = (promiseArray, apiCallFunction, ownerProject, objectPropertyName) => {
  promiseArray.push(apiCallFunction(ownerProject)
    .then(response => {
      let reply = {}

      reply[ownerProject] = {}
      reply[ownerProject][objectPropertyName] = response
      return reply
    }))
}

module.exports = {
  basicDetails: basicProjectDetails,
  punchCard,
  participation,
  contributors,
  releases,
  branches,
  buildApiCallPromiseArray,
  showError: error.show
}
