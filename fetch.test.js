jest.mock('es6-promise')
import _fetch from './fetch'
jest.mock('isomorphic-fetch')

//json: function () { return { message: 'winning' } }
describe('Fetch wrapper', () => {
  it('calls an endpoint', () => {
    global.fetch = verb => new Promise((resolve, reject) => {
      resolve({ status: 200, ok: true, body: '', headers: { 'Content-Type': 'text/json' }, json: function () { return { message: 'winning' } } })
    })
    return _fetch('GET', '/')
      .then(r => {
        expect(r.message).toBe('winning')
      })
      .catch(err => {
        expect(err).toBe()
      })
  })
  it('calls an endpoint and fails', () => {
    global.fetch = verb => new Promise((resolve, reject) => {
      // must return a resolve with a 500
      return resolve({ status: 500, ok: false, body: '', headers: { 'Content-Type': 'text/json' }, json: function () { return { message: 'failing' } } })
    })
    return _fetch('FAIL', '/')
      .then(r => {
        expect(r).toBe()
      })
      .catch(err => {
        expect(err.message).toBe('Bad response from server: 500')
      })
  })
  it('calls an endpoint return a data object', () => {
    global.fetch = verb => new Promise((resolve, reject) => {
      resolve({ status: 200, ok: true, body: '', headers: { 'Content-Type': 'text/json' }, json: function () { return { data: { message: 'winning' } } } })
    })
    return _fetch('GET', '/')
      .then(r => {
        expect(r.message).toBe('winning')
      })
      .catch(err => {
        expect(err).toBe()
      })
  })
  it('calls an endpoint times out', () => {
    global.fetch = verb => new Promise((resolve, reject) => {
      // Simulate timeout
      resolve()
    })
    return _fetch('GET', '/')
      .then(r => {
        expect(r.message).toBe('winning')
      })
      .catch(err => {
        expect(err.message).toBe('Connection to web service timed out.')
      })
  })
})
